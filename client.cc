#include <iostream>
#include <zmqpp/zmqpp.hpp>
#include <fstream>

using namespace std;
using namespace zmqpp;

int main(int argc, char const *argv[]) {
  /*if(argc != 4){
    cerr << "Error calling the program" << endl;
    return 1;
  }
*/
  // initialize the 0MQ context
  context ctx;

  // generate a push socket (canal de comunicacion)
  socket s (ctx,socket_type::request);
  /* tipo de socket
      push - enviar mensajes , pero no recibir mensajes
      pull - recibir mensajes, pero no enviar mensajes
      Para comunicacion mutua no se usara push y pull, estas se cambiaran por:
      push - REQ  (request) -> permite enviar y recibir
      pull - REP  (reply)   -> permite recibir y enviar
   */

  // open the connection
  cout << "Opening connection to " << endl;
  s.connect("tcp://localhost:4242");

  //leer matriz 1 y matriz 2
  ifstream Leermatriz1(argv[1]);
  ifstream Leermatriz2(argv[2]);

  int fila1, fila2; // fila 1 es para matriz 1
  int columna1, columna2; //columna 1 es para matriz 1
  Leermatriz1 >> fila1 >> columna1;
  Leermatriz2 >> fila2 >> columna2;

  int **matriz1;

  //Creo el tamaño de las matriz 1 de forma dinamica, ya que sera enviada a funcion
  matriz1 = (int **)malloc(fila1*sizeof(int*));

  for (int i = 0; i < fila1; i++){

    matriz1[i] = (int*)malloc(columna1*sizeof(int));

  }

  int matriz2[fila2][columna2];
  int matriz3[fila1][columna1];
  double matriz_invertida[fila1][fila1];
  int det;


  // send a message
  cout << "Sending text and a number..." << endl;
  message req;

  //envio el tamaño de las filas y columnas
  req << fila1 << columna1 << fila2 << columna2;

  //Leo y envio la matriz 1
  for(int i = 0; i<fila1; i++){
    for(int j = 0; j<columna1; j++){
      Leermatriz1 >> matriz1[i][j];
      req << matriz1[i][j];
      }
  }

  //Leo y envio la matriz 2
  for(int i = 0; i<fila2; i++){
    for(int j = 0; j<columna2; j++){
      Leermatriz2 >> matriz2[i][j];
      req << matriz2[i][j];
      }
  }

  s.send(req);

  /* TAREA
  2 binarias
  2 unarias
  multiplicacion de matrices - viernes
  inversion
  */

  // Recibir el mensaje
  cout << "Request sent." << endl;
  message rep;
  s.receive(rep);

//Recibo e imprimo matriz 1
  cout << "Matriz 1: " << endl;
  for(int i=0;i<fila1;i++){
    for(int j=0;j<columna1;j++){
        rep >> matriz1[i][j];
        cout << matriz1[i][j] << " ";
    }
    cout << endl;
  }
//Recibo e imprimo matriz 2
  cout << "Matriz 2: " << endl;
  for(int i=0;i<fila2;i++){
    for(int j=0;j<columna2;j++){
        rep >> matriz2[i][j];
        cout << matriz2[i][j] << " ";
    }
    cout << endl;
  }
//Recibo e imprimo matriz 3 multiplicacion
  if(fila1 == columna2 && fila2 == columna1){
    cout << "Multiplicacion matriz 1 y 2: " << endl;
    for(int i=0;i<fila1;i++){
      for(int j=0;j<columna2;j++){
          rep >> matriz3[i][j];
          cout << matriz3[i][j] << " ";
      }
      cout << endl;
    }
  }
  else{
    cout << "Las matrices no se pueden multiplicar" << endl;
  }

  //Recibo e imprimo matriz inversa
  if(fila1 == columna1){

    cout <<" Determinante: " << endl;
    rep >> det;
    cout << det << endl;

    if(det != 0){

      cout << "Inversa de la matriz 1: " << endl;
      for(int i=0;i<fila1;i++){
        for(int j=0;j<columna1;j++){
            rep >> matriz_invertida[i][j];
            cout << matriz_invertida[i][j] << " ";
        }
        cout << endl;
      }
    }else{

      cout << "La matriz no tiene inversa" << endl;

    }

  }

}
