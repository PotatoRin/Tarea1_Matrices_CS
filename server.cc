#include <zmqpp/zmqpp.hpp>
#include <string>
#include <iostream>
#include <math.h>
#define MAX 20

using namespace std;
using namespace zmqpp;
// Hacer hashtable para usuarios string"usuario"<string"password",bool"online">
//string"usuario",add         direcciones
// string, list<string>       amigos
void invermat(int n, int **Matriz, double Matriz_Invertida[][MAX], int &det);

int main(int argc, char *argv[]) {


  // initialize the 0MQ context
  context ctx;

  // generate a pull socket
  socket s (ctx,socket_type::reply);

  // bind to the socket
  cout << "Binding to " << endl;
  s.bind("tcp://*:4242");

  // receive the message
  cout << "Receiving message..." << endl;
  message req;
  // Recibir el mensaje enviado por el cliente
  s.receive(req);
  // Recibe suma y valores a ser sumados

  //string op;
  int fila1 = 0;
  int columna1 = 0;
  int fila2 = 0;
  int columna2 = 0;

req >> fila1 >> columna1 >> fila2 >> columna2;

int **matriz1;
//Creo el tamaño de la matriz 1 de forma dinamica
matriz1 = (int **)malloc(fila1*sizeof(int*));

for (int i = 0; i < fila1; i++){

  matriz1[i] = (int*)malloc(columna1*sizeof(int));

}

int matriz2[fila2][columna2];
int matriz3[fila1][columna2];

//para la matriz inversa
double matriz_invertida[MAX][MAX]; // aqui se guardara la matriz invertida
int det; // es el determinante de la matriz

message rep;

//Recibir matrices del cliente
if(fila1 == columna2 && fila2 == columna1){
  //matriz 1
  for(int i=0;i<fila1;i++){
    for(int j=0;j<columna1;j++){
        req >> matriz1[i][j];
    }
  }
  //matriz 2
  for(int i=0;i<fila2;i++){
    for(int j=0;j<columna2;j++){
        req >> matriz2[i][j];
    }
  }

  //multiplicacion de matrices

  for(int i=0;i<fila1;i++){
    for(int j=0;j<columna2;j++){
      matriz3[i][j] = 0;
      for(int k=0;k<columna1;k++){
        matriz3[i][j] = matriz1[i][k]*matriz2[k][j] + matriz3[i][j];
      }
    }
  }

  //envio matriz 1
  for(int i=0;i<fila1;i++){
    for(int j=0;j<columna1;j++){
        rep << matriz1[i][j];
    }
  }
  //envio matriz 2
  for(int i=0;i<fila2;i++){
    for(int j=0;j<columna2;j++){
        rep << matriz2[i][j];
    }
  }
  //envio matriz 3
  for(int i=0;i<fila1;i++){
    for(int j=0;j<columna2;j++){
        rep << matriz3[i][j];
    }
  }

}



  //Matriz inversa
  if(fila1 == columna1){

    	invermat(fila1, matriz1, matriz_invertida, det);

      if(det == -0){
        det == 0;
      }
      cout << det;

      rep << det;


      for(int i = 0; i < fila1; i++){

        for(int j = 0; j < columna1; j++){

            rep << matriz_invertida[i][j];

        }

      }

    }
    s.send(rep);
    cout << "Finished." << endl;
}

//Calculo matriz inversa usando Gauss
void invermat(int n, int **Matriz, double Matriz_Invertida[][MAX], int &det) {

// Algoritmo para la eliminación simple de Gauss

  int i, j, k;

  double factor;
  double L[MAX][MAX], D[MAX], X[MAX];

  for (k = 0; k < n - 1; k++) {

    for (i = k+1; i < n;  i++) {

      factor = Matriz[i][k]/Matriz[k][k];

      for (j = k+1; j < n + 1; j++) {

        Matriz[i][j] = Matriz[i][j] - factor * Matriz[k][j];

      }

    }

  }

// Cálculo del determinante

    det = 1.;

  for (i = 0; i < n; i++) {
    det = det * Matriz[i][i];
  }

  if (det != 0) {

  // Rutina para determinar las matrices L (inferior) y U (superior) de la
  // descomposición LU


      for (i = 0; i < n; i++) {

          for (j = 0; j < n; j++) {

              if (i > j) {

                  L[i][j] = Matriz[i][j]/Matriz[j][j];

                  Matriz[i][j] = 0;

              }

          }

      }


      for (i = 0; i < n; i++) {

          for (j = 0; j < n; j++) {

              L[j][j] = 1;

          }

      }


  // Implementación de la rutina para el cálculo de la inversa


    for (k = 0; k < n; k++) {


  // Esta rutina inicializa los L[i][n] para ser utilizados con la matriz L


        for (i = 0; i < n; i++) {

              if (i == k) L[i][n] = 1;

              else  L[i][n] = 0;

          }

  // Esta función implementa la sustitución hacia adelante con la matriz L y los L[i][n]
  // que produce la rutina anterior

        double suma;

        D[0] = L[0][n];

        for (i = 1; i < n; i++) {

           suma = 0.0;

           for (j = 0; j < i; j++) {

                suma = suma + L[i][j]*D[j];

           }

            D[i] = L[i][n] - suma;

        }



    // Esta rutina asigna los D[i] que produce forward para ser utilizados con la matriz U

        for (i = 0; i < n; i++) {

              Matriz[i][n] = D[i];

        }

    // Rutina que aplica la sustitución hacia atras


      X[n-1] = Matriz[n-1][n]/Matriz[n-1][n-1];

     // Determinación de las raíces restantes


        for (i = n - 2; i > -1; i--) {

            suma = 0.0;

            for (j = i+1; j < n; j++) {

                  suma = suma + Matriz[i][j]*X[j];

             }

             X[i] = (Matriz[i][n] - suma)/Matriz[i][i];

        }


    // Esta rutina asigna los X[i] que produce Sustituir como los elementos de la matriz inversa

        for (i = 0; i < n; i++) {

             Matriz_Invertida[i][k] = X[i];

        }

    }   // llave de cierre del for para k

  }   // cierre del if

}
